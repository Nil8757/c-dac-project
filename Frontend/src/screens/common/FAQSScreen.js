import Navigation from "../../components/Navigation";

const FAQSScreen = () => {
  return (
    <div>
      <Navigation />
      <div className="main1">
        <h3>FAQs : </h3>
        <p>
          Kindly check the FAQ below if you are not very familiar with the
          functioning of this website. If your query is of urgent nature and is
          different from the set of questions then do write to us at
          customerservice@emobilestore.in or call us on 8855042644 between 10 am
          & 5 pm on all days including Sunday to get our immediate help.
        </p>
        <h5>Registration : </h5>
        <h5>How do I register?</h5>
        <p>
          You can register by clicking on the “Sign Up” link at the top right
          corner of the homepage. Please provide the information in the form
          that appears. You can review the terms and conditions, provide your
          payment mode details and submit the registration information.{" "}
        </p>
        <h5>Is it necessary to have an account to shop on E-Mobile Store?</h5>
        <p>
          Yes, it's necessary to log into your E-Mobile Store account to shop. Shopping
          as a logged-in user is fast & convenient.
        </p>
      </div>
    </div>
  );
};
export default FAQSScreen;
