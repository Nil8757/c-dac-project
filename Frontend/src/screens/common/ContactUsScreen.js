import Navigation from "../../components/Navigation";
import Footer from "../../components/Footer"; 

const ContactUsScreen = () => {
    return (
        <div>
            <Navigation/>
            <div className="main btn4">
                <h3>Contact Us : </h3>
                <h5>Email : emobilestore@gmail.com</h5>
                <h5>Contact No : +91 8855042644</h5>
            </div> 
        </div>    
    );
}
export default ContactUsScreen