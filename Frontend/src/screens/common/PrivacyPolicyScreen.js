import Navigation from "../../components/Navigation";

const PrivacyPolicyScreen = () => {
  return (
    <div>
      <Navigation />
      <div className="main1">
        <h3>Privacy Policy : </h3>
        <h5>Personal Information</h5>
        <p>
          We value the trust you place in us and recognize the importance of
          secure transactions and information privacy. This Privacy Policy
          describes how  and its affiliates
          collect, use, share or
          otherwise process your personal information through  website
          www.flipkart.com, its mobile application, and m-site (hereinafter
          referred to as the “Platform”). By visiting the Platform, providing
          your information or availing our product/service, you expressly agree
          to be bound by the terms and conditions of this Privacy Policy, the
          Terms of Use and the applicable service/product terms and conditions.
          If you do not agree, please do not access or use our Platform.{" "}
          <h6>1. Collection of Your Information</h6> When you use our Platform,
          we collect and store your information which is provided by you from
          time to time. In general, you can browse the Platform without telling
          us who you are or revealing any personal information about yourself.
          Once you give us your personal information, you are not anonymous to
          us. Where possible, we indicate which fields are required and which
          fields are optional. You always have the option to not provide
          information by choosing not to use a particular service, product or
          feature on the Platform. We may track your buying behaviour,
          preferences, and other information that you choose to provide on our
          Platform. We use this information to do internal research on our
          users' demographics, interests, and behaviour to better understand,
          protect and serve our users. This information is compiled and analysed
          on an aggregated basis. This information may include the URL that you
          just came from (whether this URL is on our Platform or not), which URL
          you next go to (whether this URL is on our Platform or not), your
          computer browser information, and your IP address. We collect personal
          information (such as email address, delivery address, name, phone
          number, credit card/debit card and other payment instrument details)
          from you when you set up an account or transact with us. While you can
          browse some sections of our Platform without being a registered
          member, certain activities (such as placing an order or consuming our
          online content or services) do require registration. We do use your
          contact information to send you offers based on your previous orders
          and your interests. If you choose to post messages on our message
          boards, chat rooms or other message areas or leave feedback or if you
          use voice commands to shop on the Platform, we will collect that
          information you provide to us. We retain this information as necessary
          to resolve disputes, provide customer support and troubleshoot
          problems as permitted by law. If you send us personal correspondence,
          such as emails or letters, or if other users or third parties send us
          correspondence about your activities or postings on the Platform, we
          may collect such information into a file specific to you. If you enrol
          into our SuperCoins Loyalty Program, we will collect and store your
          personal information such as name, contact number, email address,
          communication address, date of birth, gender, zip code, lifestyle
          information, demographic and work details which is provided by you to
          or a third-party business partner that operates
          online/offline establishments or platforms where you can earn
          SuperCoins for purchase of goods and services, and redeem SuperCoins.
          We will also collect your information related to your transactions on
          platform and such third-party business partner platforms.
          When such third-party business partner collects your personal
          information directly from you, you will be governed by their privacy
          policies.  shall not be responsible for the third-party
          business partner’s privacy practices or the content of their privacy
          policies, and we request you to read their privacy policies prior to
          disclosing any information.  has onboarded certain third-party
          business partners on the Platform who specialise in the categories
          like travel ticket reservations, booking online movie tickets, paying
          online bills and more (Ultra-Partners). If you use the services of
          Ultra-Partners, you will be redirected to Ultra-Partners
          websites/applications and your entry to Ultra-Partners
          websites/applications will be based on your  login credentials
          after seeking your permissions to share the data further. Flipkart
          shall not be responsible for the Ultra-Partner’s privacy practices or
          the content of their privacy policies, and we request you to read
          their privacy policies prior to disclosing any information.
          <br></br>
          <h6>2. Use of Demographic / Profile Data / Your Information </h6>
          We use your personal information to provide the product and services
          you request. To the extent we use your personal information to market
          to you, we will provide you the ability to opt-out of such uses. We
          use your personal information to assist sellers and business partners
          in handling and fulfilling orders; enhancing customer experience;
          resolve disputes; troubleshoot problems; help promote a safe service;
          collect money; measure consumer interest in our products and services;
          inform you about online and offline offers, products, services, and
          updates; customize and enhance your experience; detect and protect us
          against error, fraud and other criminal activity; enforce our terms
          and conditions; and as otherwise described to you at the time of
          collection of information. With your consent, we will have access to
          your SMS, contacts in your directory, location and device information.
          We may also request you to provide your PAN, GST Number, Government
          issued ID cards/number and Know-Your-Customer (KYC) details to: (i)
          check your eligibility for certain products and services including but
          not limited to credit and payment products; (ii) issue GST invoice for
          the products and services purchased for your business requirements;
          (iii) enhance your experience on the Platform and provide you access
          to the products and services being offered by us, sellers, affiliates
          or lending partners. You understand that your access to these
          products/services may be affected in the event consent is not provided
          to us. In our efforts to continually improve our product and service
          offerings, we and our affiliates collect and analyse demographic and
          profile data about our users' activity on our Platform. We identify
          and use your IP address to help diagnose problems with our server, and
          to administer our Platform. Your IP address is also used to help
          identify you and to gather broad demographic information. We will
          occasionally ask you to participate in optional surveys conducted
          either by us or through a third party market research agency. These
          surveys may ask you for personal information, contact information,
          date of birth, demographic information (like zip code, age, or income
          level), attributes such as your interests, household or lifestyle
          information, your purchasing behaviour or history, preferences, and
          other such information that you may choose to provide. The surveys may
          involve collection of voice data or video recordings, the
          participation of which would purely be voluntary in nature. We use
          this data to tailor your experience at our Platform, providing you
          with content that we think you might be interested in and to display
          content according to your preferences.
        </p>
        <h5></h5>
      </div>
    </div>
  );
};
export default PrivacyPolicyScreen;
